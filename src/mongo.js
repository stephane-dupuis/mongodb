import mongoose from 'mongoose'
import {userSchema} from './schemas'

 const url = 'mongodb://localhost:27017/local'
 const options = {
 	promiseLibrary: Promise,
 }

 mongoose.connect(url, options)

 mongoose.connection.on('connected', () =>
 	console.log ("[mongoDB] is running on port 27017")
 )

 mongoose.connection.on('disconnected', () =>
 	console.warn ("[mongoDB] is disconnected on port 27017")
 )

// Définition du nom du schéma
mongoose.model('User', userSchema)