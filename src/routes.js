import express from 'express'
import mongoose from 'mongoose'
import { createHash } from 'crypto'

// Utilisation du schéma défini dans mongo.js
const User = mongoose.model('User')

const Router = express.Router()

Router.post('/user', (req,res) => {
	const {email, password } = req.body
	console.log (password)
	const safePwd = createHash("whirlpool").update(password).digest("base64")
	const user = new User({email, password: safePwd	})

	user
		.save()
		.then(account => {
			return res.send('User created')
		})
		.catch(({errmsg }) => {
			return err.status(500).send(errmsg)
		})
})	

Router.get('/', (req, res) => 
	res.send("ca fonctionne")
)

Router.get('/users', async(req, res) => {
	try {
		const users = await User.find().exec()

		if (!users)
			throw new Error('Aucun utilisateurs')
		return res.json(users)
	} catch (e){
		console.error(e)
	}
	res.send("ca fonctionne" + users)
})

Router.get('/user/:id', async(req, res) => {
	try {
		const user = await User.findOne({_id: req.params.id}).exec()

		if (!user)
			throw new Error('Utilisateur non trouvé')
			return res.json(user)
	} catch (e){
		console.error(e)
	}
	res.send(user.name)
})

export default Router
