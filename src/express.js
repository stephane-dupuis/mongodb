import express from 'express'
import bodyParser from 'body-parser'

import Router from './routes'

const SERVER_PORT = "6789"

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

app.use ('/api',Router)

app.listen(SERVER_PORT, () =>
	console.log ("[Express] is running on port "+SERVER_PORT)
)